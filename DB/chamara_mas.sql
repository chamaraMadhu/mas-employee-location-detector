-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jan 23, 2019 at 07:34 PM
-- Server version: 5.6.35
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `chamara_mas`
--

-- --------------------------------------------------------

--
-- Table structure for table `tblemployee`
--

CREATE TABLE IF NOT EXISTS `tblemployee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `district` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gampaha_district` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `home_line` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobi1` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobi2` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `map_description` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `emp_id` (`emp_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=34 ;

--
-- Dumping data for table `tblemployee`
--

INSERT INTO `tblemployee` (`id`, `emp_id`, `title`, `name`, `designation`, `image`, `address`, `district`, `gampaha_district`, `province`, `email`, `home_line`, `mobi1`, `mobi2`, `map`, `map_description`) VALUES
(20, '5142', 'Miss.', 'D. M. S. Wasana', 'Team member', 'female_default.jpg', 'Siri Medura Henyaya, Wilayaya, Dampahala', 'Matara', '', 'Southern', '', '0417922643', '0789683824', '', '<iframe src="https://www.google.com/maps/d/embed?mid=18_JPBxMsBN-zs4Hqrab6GtRQ0278Tbuj" width="640" height="480"></iframe>', ''),
(21, '5143', 'Mrs.', 'H. A. Chathurika', 'Team member', 'female_default.jpg', '488, Peragas Junction, Biyagama', 'Gampaha', '', 'Western', '', '', '0758912488', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1sX_Y35QiLwH-aXfhRyFyiba7Z-x8c8g9" width="640" height="480"></iframe>', ''),
(22, '5144', 'Mrs.', 'H. K. W. G. Niluka damayanthi', 'Team member', 'female_default.jpg', '82, 19/A, Pugollawatta, Kithalawala', 'Kurunegala', '', 'North Western', '', '0782049208', '', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1v4aZqZyCQ0Q44tYVFSQ3YNm0Lcrex0Oa" width="640" height="480"></iframe>', ''),
(23, '5145', 'Mrs.', 'V. Samdamali', 'Team member', 'female_default.jpg', '523, Daranagama, Delgoda', 'Gampaha', '', 'Western', '', '0719437793', '', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1db_GDpBjEcJDk0F_qhdRxa0eFZlEHCH1" width="640" height="480"></iframe>', ''),
(24, '5146', 'Mrs.', 'W. N. D. Peiris', 'Team member', 'female_default.jpg', '418/1 D, Kottunna road, Biyagama', 'Gampaha', 'Biyagama', 'Western', '', '0711546427', '', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1A-83590a3Yxnke-bpN8fIPVyezMlYaJ5" width="640" height="480"></iframe>', 'Go stratight from thathsara Communication for about 1 kilo meter. Turn left from the godapara gaha watta road.  from there the 6th house on the left.'),
(25, '5148', 'Mrs.', 'A. A. Nadeesha Erandi', 'Team member', 'female_default.jpg', '214, Biyanwila Down, Kadawatha', 'Gampaha', 'Biyagama', 'Western', '', '', '0779607878', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1tYj5Iyg_ByU2pHFghOtEDMfSwcyWkmBO" width="640" height="480"></iframe>', ''),
(26, '5149', 'Mrs.', 'B. N. Sewwandi Kumari', 'Team member', 'female_default.jpg', '184/3, Samelwatta, Malwana', 'Gampaha', 'Dompe', 'Western', '', '', '0762661031', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1TrbekFcqg6LNMiazP3k3cUykXuvzhQLz" width="640" height="480"></iframe>', ''),
(27, '5155', 'Mrs.', 'S. H> A. Niluka Kumari', 'Team member', 'female_default.jpg', '205/3, Kandawatta, Mandawala', 'Gampaha', 'Dompe', 'Western', '', '', '0761240665', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1EL2gjBlDs8AP-a57c-IC-FFXu3ogFitB" width="640" height="480"></iframe>', ''),
(28, '5156', 'Miss.', 'K. A. Maheshika Madushani Jayaratne', 'Team member', 'female_default.jpg', '58/115 B, Rathupaswala Mudungoda', 'Gampaha', 'Gampaha', 'Western', '', '', '0758530561', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1tiFpqQSnrOVnG1_U04o4zxsBHW3aF65g" width="640" height="480"></iframe>', ''),
(29, '5157', 'Mrs.', 'P. G. R Nilusha Podimanike', 'Team member', 'female_default.jpg', '347/2,Alpalavilawatta, Vihara kumbura, Kirindiwela', 'Gampaha', 'Dompe', 'Western', '', '', '0788397194', '', '<iframe src="https://www.google.com/maps/d/embed?mid=13LT3bUk9xjh5vL-WVgq4l1LwyXknKthT" width="640" height="480"></iframe>', ''),
(30, '5158', 'Mrs.', 'H.A M. Sandya Kumari', 'Team member', 'female_default.jpg', '168/3, Indalamulla , Dompe', 'Gampaha', 'Dompe', 'Western', '', '', '0715951540', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1350ijuDx1Q9UnCeY9WAyqrqK9GYVYEj-" width="640" height="480"></iframe>', ''),
(31, '5159', 'Mrs.', 'W. T. Suranji Weerakkodi', 'Team member', 'female_default.jpg', '18, Lunugama , Mandawala', 'Gampaha', 'Dompe', 'Western', '', '', '0710155720', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1vhKkqerueR8ied2gZKGDwxM01ibVU73Y" width="640" height="480"></iframe>', ''),
(32, '5164', 'Mr.', 'Aravinda Ernest', 'Team member', 'male_default.jpg', '16A/a, Neegrodharam road. Kandy road, Peliyagoda', 'Colombo', '', 'Western', '', '', '0783361026', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1qUP5-FumyaeU3WaY48y-jS4QeqSYhiz7" width="640" height="480"></iframe>', ''),
(33, '5162', 'Miss.', 'J. R. Udeshika Lakmali', 'Team member', 'female_default.jpg', '173/21, Pamunuwila Gonawala, Kelaniya', 'Colombo', '', 'Western', '', '', '0789932146', '', '<iframe src="https://www.google.com/maps/d/embed?mid=1BVF0baChJv-iS5UZVjGapCGzWps_18M9" width="640" height="480"></iframe>', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `addition` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `edit` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deletion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `user_name`, `role`, `email`, `password`, `image`, `addition`, `view`, `edit`, `deletion`) VALUES
(2, 'Neluwe Liyanage Chamara Madhushanka', 'Admin', 'nlc.madhushanka@gmail.com', 'Y2NjYw==', 'FB_IMG_15048716616161155.jpg', '1', '1', '1', '1'),
(5, 'Madhavi Watawala', 'Manager', 'madhavi@gmail.com', 'bW1tbQ==', '1.png', '1', '1', '1', '1'),
(10, 'Shenali', 'HR Intern', 'shenaliwickramarathne@gmail.com', 'c3Nzcw==', 'female_default.jpg', '1', '1', '1', '1');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
