<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['view']==0){
	header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
        <div class="col-10 bg-danger p-0">
        	<div class="container-fluid p-0">

		    <nav aria-label="breadcrumb">
		        <ol class="breadcrumb bg-danger" style="font-size: 14px">
					<li class="breadcrumb-item text-light active" aria-current="page">Employee</li>
		            <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_employee.php" style="text-decoration: none">View Employee</a></li>
		            <li class="breadcrumb-item text-light active" aria-current="page"><a class="text-light" href="employee_info.php" style="text-decoration: none">Details of Employee</a></li>
		        </ol>
		    </nav>

			    <div class="container-fluid">					
			        <h2 class="mt-2">DETAILS OF EMPLOYEE</h2><hr>
		            
		            <?php 
		            // aleart massages for editing product
		            if(isset($_GET['succcess_pro_edit_msg'])){ ?>

		                <div class="alert alert-success alert-block">
		                  <button type="button" class="close" data-dismiss="alert">x</button>
		                  <strong><?php echo $_GET['succcess_pro_edit_msg']; ?></strong> 
		                </div>
		            <?php 
		                }
		                // aleart massages for editing product 


		            // aleart massages for deleting product
		              if(isset($_GET['success_del_msg'])){
		            ?>
		                <div class="alert alert-success alert-block">
		                    <button type="button" class="close" data-dismiss="alert">x</button>
		                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
		                </div>

		            <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

		                <div class="alert alert-danger alert-block">
		                  <button type="button" class="close" data-dismiss="alert">x</button>
		                  <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
		                </div>
		            <?php 
		                }
		                // aleart massages for deleting product
		             ?>

		            <div class="container-fluid bg-white border mb-4">
						
						<?php

						include "inc/db.conn.php";

							$id = $_GET['id']; 

							$get_employee = "SELECT * FROM tblemployee WHERE id = '$id'";
                          	$run_employee = mysqli_query($con,$get_employee);

                         	while($res_employee = mysqli_fetch_array($run_employee)){

						?>
							<div class="row">
								<table class="table text-left col-12">
								  <tbody>
								    <tr>
								      <td style="width: 175px"><img src="../img/employee/<?php echo $res_employee['image'] ?>" width="150px" height="180px" /></td>
								      <td>
								      	<h3 class="text-capitalize text-dark mb-0"><?php echo $res_employee['title'].' '.$res_employee['name'] ?></h3>
								      	
								      	<h5 class="text-capitalize mb-0"><b>Designation : <?php echo $res_employee['designation'] ?></b></h6>
								      	<p class="text-capitalize" style="margin-bottom: -20px"><b>EmpNo : <?php echo $res_employee['emp_id'] ?></b></p><br/>
								      	<span class="text-capitalize text-muted" style="font-size: 14px; float: left"><b>Address :</b> <?php echo $res_employee['address'] ?></span><br/>
								      	<span class="text-capitalize text-muted" style="font-size: 14px; float: left"><b>district :</b> <?php echo $res_employee['district']; if($res_employee['district']=='Gampaha') echo ' - '.$res_employee['gampaha_district'] ?> </span><br/>
								      	<span class="text-capitalize text-muted" style="font-size: 14px; float: left"><b>province :</b> <?php echo $res_employee['province'] ?> </span>
								      	

								      </td>
								      <td>
								      	<table class="table table-sm" style="border-style: bold; border-color: #fff">
								      		<tr>

								      			<td><i class="fa fa-envelope"> </i></td>
								      			<td class="text-muted" style="font-size: 14px"><?php echo $res_employee['email'] ?></td>

								      		</tr>
								      		<tr>
								      			<td><i class="fa fa-phone" aria-hidden="true"></i></td>
								      			<td class="text-muted" style="font-size: 14px"><?php echo $res_employee['home_line'] ?></td>
								      		</tr>
								      		<tr>
								      			<td><i class="fa fa-mobile 3x"> </i></td>
								      			<td class="text-muted" style="font-size: 14px"><?php echo $res_employee['mobi1'] ?> </td>
								      		</tr>
								      		<tr>
								      			<td></td>
								      			<td class="text-muted" style="font-size: 14px"><?php echo $res_employee['mobi2'] ?> </td>
								      		</tr>
								      	</table>



								      </td>
								    </tr>
								    
								  </tbody>
								</table>
							</div>

							<!-- map description-->
							<div class="row mb-2">
						      	<span class="text-capitalize text-muted ml-3" style="font-size: 14px; float: left"><b>More Details of Location :</b> <?php echo $res_employee['map_description'] ?></span>
						    </div>
						    <!-- map description-->

							<!-- map -->
							<div class="row mb-2">
						      	<?php echo $res_employee['map'] ?>
						    </div>
						    <!-- map -->
							<?php } ?>
			        </div>
			    </div>      
			</div>
		</div>
        <!-- content -->

<?php 
  include "inc/footer.php";
?>