<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['role']<>'Manager'){
    header('location:dashboard.php');
 }
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-10 bg-danger p-0">
     <div class="page-wrapper">

    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-danger" style="font-size: 14px">
			   <li class="breadcrumb-item text-light active" aria-current="page">Setting</li>
         <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="view_user.php" style="text-decoration: none">View User</a></li>
        </ol>
    </nav>

    <div class="container-fluid">

		<h2>SETTING</h2><hr>
      <?php
        if(isset($_GET['success_edit_msg'])){
      ?>
                  <div class="alert alert-success alert-block">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong><?php echo $_GET['success_edit_msg']; ?> </strong> 
                  </div>

      <?php }elseif(isset($_GET['fail_edit_msg'])){ ?>

          <div class="alert alert-danger alert-block">
                      <button type="button" class="close" data-dismiss="alert">x</button>
                      <strong><?php echo $_GET['fail_edit_msg']; ?></strong> 
                  </div>

      <?php 
      }

				if(isset($_GET['success_del_msg'])){
			?>
	                <div class="alert alert-success alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
	                </div>

			<?php }elseif(isset($_GET['fail_del_msg'])){ ?>

					<div class="alert alert-danger alert-block">
	                    <button type="button" class="close" data-dismiss="alert">x</button>
	                    <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
	                </div>

			<?php } ?>

		<div class="container-fluid bg-white ">
			     <div class="row pt-2 " style="background-color: gray">
                <h5 class="col-12 text-white">VIEW USER</h5>
            </div>

  	        <div class="mb-4">
                  <div class="card-body">
                      
                    <div class="table-responsive">
                        <table id="zero_config" class="table table-hover">
                            <thead>
                                <tr style="background-color: gray; color: #fff">
                                    <td>User Name</td>
                                    <td>Image</td>
                                    <td>Role</td>
                                    <td>Mail</td>
                                    <td>Password</td>
                                    <td>Privileges</td>
                                    <?php if (isset($_SESSION['email'])){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
                                    <td>Action</td>
                                    <?php } }?>
                                </tr>
                            </thead>
                            <tbody>

                            <?php

                              include "inc/db.conn.php";

                              $get_user = "SELECT * FROM user WHERE id <>'$_SESSION[id]'";

                              $run_user = mysqli_query($con,$get_user);

                              while($res_user = mysqli_fetch_array($run_user)){

                                $id = $res_user['id'];
                                $uname = $res_user['user_name'];
                                $img = $res_user['image'];
                                $role = $res_user['role'];
                                $mail = $res_user['email'];
                                $pwd = base64_decode($res_user['password']);
                                $add = $res_user['addition'];
                                $view= $res_user['view'];
                                $edit = $res_user['edit'];
                                $del = $res_user['deletion'];
                              ?>

                                	<tr>
  	                                  <td><?php echo $uname; ?></td>
  	                                  <td align="center"><img src="../img/user/<?php echo $img; ?>" width="60px" height="70px" class="py-1"></td>
  	                                  <td><?php echo $role; ?></td>
                                      
  	                                  <td><?php if ($_SESSION['del']=='1'){ ?><?php echo $mail; ?> <?php }else echo "Access denied" ?></td>

  	                                  <td><?php if ($_SESSION['del']=='1'){ ?><?php echo $pwd; ?>  <?php }else echo "Access denied" ?></td>
  	                                  <td><?php if($add=='1') echo 'add' ?> <?php if($view=='1') echo 'view' ?> <?php if($edit=='1') echo 'edit' ?> <?php if($del=='1') echo 'del' ?></td>
                                    
                                      <?php if (isset($_SESSION['email'])){ if ($_SESSION['edit']=='1' || $_SESSION['del']=='1'){ ?>
    	                                  <td>
                                        	<?php if ($_SESSION['edit']=='1'){ ?>
      	                                  	<form method="GET" action="edit_user.php" style="float: left">
    	                                        <input type="hidden" name="id" value="<?php echo $id ?>"/>
    	                                      	<button href="edit_category.php" type="submit" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></button>&nbsp;
    	                                      </form>
                                          <?php } ?>

                                          <?php if ($_SESSION['del']=='1'){ ?>
    	                                      <form method="GET" action="setting/delete_user_query.php" style="float: left">
    	                                        <input type="hidden" name="id" value="<?php echo $id; ?>"/>
    	                                      	<button type="submit" name="submit" class="btn btn-danger btn-sm del_user"><i class="fa fa-trash"></i></button>
    	                                      </form>
                                          <?php }?>
    	                                  </td>
                                      <?php } }?>

  									         <?php } ?>
  					                   </tr>
                            </tfoot>
                        </table>
                    </div>

                  </div>
            </div>
  		</div>
  	</div>


    </div>      
  </div>
</div>
<!-- content -->

<?php 
  include "inc/footer.php";
?>