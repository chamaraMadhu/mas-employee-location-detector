<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['view']==0){
    header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
        <div class="col-10 bg-danger p-0">
            <div class="container-fluid p-0">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb bg-danger" style="font-size: 14px">
                    <li class="breadcrumb-item text-light active" aria-current="page">Employee</li>
                    <li class="breadcrumb-item text-light active" aria-current="page">Search Employee</li>
                </ol>
            </nav>

                <div class="container-fluid">                   
                    <h2>EMPLOYEE</h2><hr>
                    
                    <?php 
                    // aleart massages for editing product
                    if(isset($_GET['succcess_pro_edit_msg'])){ ?>

                        <div class="alert alert-success alert-block">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong><?php echo $_GET['succcess_pro_edit_msg']; ?></strong> 
                        </div>
                    <?php 
                        }
                        // aleart massages for editing product 


                    // aleart massages for deleting product
                      if(isset($_GET['success_del_msg'])){
                    ?>
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">x</button>
                            <strong><?php echo $_GET['success_del_msg']; ?> </strong> 
                        </div>

                    <?php }elseif(isset($_GET['fail_del_msg'])){ ?>

                        <div class="alert alert-danger alert-block">
                          <button type="button" class="close" data-dismiss="alert">x</button>
                          <strong><?php echo $_GET['fail_del_msg']; ?></strong> 
                        </div>
                    <?php 
                        }
                        // aleart massages for deleting product
                     ?>

                    <div class="container-fluid bg-white mb-4">

                        <div class="row pt-2" style="background-color: gray">
                            <h6 class="col-12 text-white">SEARCH EMPLOYEE</h6>
                        </div>

                        <div class="">
                                    <div class="card-body">
                                        
                                        <div class="table-responsive">
                                            <table id="zero_config" class="table table-hover">
                                                <thead>
                                                    <tr style="background-color: gray; color:#fff">
                                                        <td style="width: 55px">Emp ID</td>
                                                        <td>Name</td>
                                                        <td>Designation</td>
                                                        <td>Image</td>
                                                        <td>Address</td>
                                                        <td class="text-center" style="width: 75px">Action</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php

                                                      include "inc/db.conn.php";

                                                      $get_employee = "SELECT * FROM tblemployee WHERE emp_id = '$_GET[emp_id]'";
                                                      $run_employee = mysqli_query($con,$get_employee);

                                                      while($res_employee = mysqli_fetch_array($run_employee)){

                                                      ?>

                                                    <tr>
                                                        <td align="center"><?php echo $res_employee['emp_id'] ?></td>
                                                        <td><?php echo $res_employee['name'] ?></td>
                                                        <td><?php echo $res_employee['designation'] ?></td>
                                                        <td align="center"><img src="../img/employee/<?php echo $res_employee['image'] ?>" width="60px" height="70px"></td>
                                                        <td><?php echo $res_employee['address'] ?></td>
                                                        <td>
                                                            <?php if (isset($_SESSION['email'])){ if ($_SESSION['view']=='1'){ ?>
                                                            <form method="GET" action="employee_info.php" style="float: left">
                                                                <input type="hidden" name="id" value="<?php echo $res_employee['id'] ?>"/>
                                                                <button type="submit" title="view" name="submit" class="btn btn-info btn-sm mr-1"><i class="fa fa-info"></i></button>
                                                            </form>
                                                            <?php } } ?>

                                                            <?php if (isset($_SESSION['email'])){ if ($_SESSION['role']<>'Manager'){ ?>
                                                                <?php if ($_SESSION['edit']=='1'){ ?>
                                                                <form method="GET" action="edit_employee.php" style="float: left">
                                                                    <input type="hidden" name="id" value="<?php echo $res_employee['id'] ?>"/>
                                                                    <button type="submit" title="edit" class="btn btn-secondary btn-sm mr-1"><i class="fa fa-pencil"></i></button>
                                                                </form>
                                                                <?php } ?>

                                                                <?php if ($_SESSION['del']=='1'){ ?>
                                                                <form method="GET" action="employee/delete_employee_query.php" style="float: left">
                                                                    <input type="hidden" name="id" value="<?php echo $res_employee['id'] ?>"/>
                                                                    <button type="submit" title="delete" name="submit" class="btn btn-danger btn-sm del_emp"><i class="fa fa-trash"></i></button>
                                                                </form>
                                                                <?php } ?>
                                                            <?php } } } ?>
                                                        </td>
                                                    </tr>
                                                    
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                        </div>

                    </div>
                </div>      
            </div>
        </div>
        <!-- content -->

<?php 
  include "inc/footer.php";
?>