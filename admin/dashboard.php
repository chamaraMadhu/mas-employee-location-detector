<?php
session_start();
if(!isset($_SESSION['email'])){
	header('location:login.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
<div class="col-sm-3 col-md-10 bg-danger p-0">
    <div class="page-wrapper">

	  	<nav aria-label="breadcrumb">
	      	<ol class="breadcrumb bg-danger" style="font-size: 14px">
	          	<li class="breadcrumb-item text-light"><a class="text-light" href="dashboard.php" style="text-decoration: none">Dashboard</a></li>
	      	</ol>
	  	</nav>

      <div class="container-fluid">

      <h2>STATISTICS</h2><hr>

        <div class="container-fluid bg-white mb-4 p-2">

        <?php 

        include "inc/db.conn.php";

        $tot_employee = "SELECT * FROM tblemployee";
        $run_tot_employee = mysqli_query($con,$tot_employee);
        $res_tot_employee = mysqli_num_rows($run_tot_employee);

        $tot_gampaha_employee = "SELECT * FROM tblemployee WHERE district='Gampaha'";
        $run_tot_gampaha_employee = mysqli_query($con,$tot_gampaha_employee);
        $res_tot_gampaha_employee = mysqli_num_rows($run_tot_gampaha_employee);

        ?>

        <!-- Genderwise -->
        <table class="table">
          <tr style="background-color: gray; color: #fff" align="center"><td colspan="4">Gender wise</td></tr>
          
          <?php 

          $tot_male = "SELECT * FROM tblemployee WHERE title = 'Mr.'";
          $run_male = mysqli_query($con,$tot_male);
          $res_male = mysqli_num_rows($run_male);

          $tot_female = "SELECT * FROM tblemployee WHERE title = 'Miss.' OR title = 'Mrs.' OR title = 'Ms.'";
          $run_female = mysqli_query($con,$tot_female);
          $res_female = mysqli_num_rows($run_female);

          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Gender</td>
            <td width="175px">No. of Employees</td>
            <td width="175px">%</td>
            <td rowspan="4" bgcolor="#fff"><div id="piechart" style="width: auto; height: auto"></div></td>
          </tr>

          <tr align="center">
            <td>Male</td>
            <td><?php echo $res_male ?></td>
            <td><?php if($res_tot_employee!==0) echo round(($res_male/$res_tot_employee )*100, 2)  ?>%</td>
          </tr>
          <tr align="center">
            <td>Female</td>
            <td><?php echo $res_female ?></td>
            <td><?php if($res_tot_employee!==0) echo round(($res_female/$res_tot_employee )*100, 2) ?>%</td>
          </tr>
          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_employee ?></td>
            <td>100%</td>
          </tr>
        </table>
        <!-- Genderwise -->

        <!-- Disttrict wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">District Wise</td></tr>

          <?php 

          $get_district_row = "SELECT district, count(district) FROM tblemployee GROUP BY district";
          $run_district_row = mysqli_query($con,$get_district_row);
          $res_district_row = mysqli_num_rows($run_district_row);

          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">District</td>
            <td width="175px">No. of Employees</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_district_row + 2 ) ?>" bgcolor="#fff"><div id="piechart1" style="width: 100%; min-height: <?php echo (($res_district_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_district = "SELECT district, count(district) AS count_dis FROM tblemployee GROUP BY district";
          $run_district = mysqli_query($con,$get_district );

          while($res_district = mysqli_fetch_array($run_district )){

          ?>

          <tr align="center">
            <td><?php echo $res_district['district'] ?></td>
            <td><?php echo $res_district['count_dis'] ?></td>
            <td><?php echo round(($res_district['count_dis']/$res_tot_employee*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_employee ?></td>
            <td>100%</td>
          </tr>

        </table>
        <!-- Disttrict wise -->
        
        <!-- Gampha District -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Gampaha District</td></tr>

          <?php 

          $get_gampaha_district_row = "SELECT gampaha_district, count(gampaha_district) FROM tblemployee GROUP BY gampaha_district";
          $run_gampaha_district_row = mysqli_query($con,$get_gampaha_district_row);
          $res_gampaha_district_row = mysqli_num_rows($run_gampaha_district_row);

          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Pradeshiya Saba</td>
            <td width="175px">No. of Employees</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_gampaha_district_row + 2 ) ?>" bgcolor="#fff"><div id="piechart2" style="width: 100%; min-height: <?php echo (($res_gampaha_district_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_gampaha_district = "SELECT gampaha_district, count(gampaha_district) AS count_gampaha_dis FROM tblemployee GROUP BY gampaha_district";
          $run_gampaha_district = mysqli_query($con,$get_gampaha_district );

          while($res_gampaha_district = mysqli_fetch_array($run_gampaha_district )){

          ?>

          <tr align="center">
            <?php if(empty($res_gampaha_district['gampaha_district'])){

            }else{ 

            ?>

            <td><?php echo $res_gampaha_district['gampaha_district'] ?></td>
            <td><?php echo $res_gampaha_district['count_gampaha_dis'] ?></td>
            <td><?php echo round(($res_gampaha_district['count_gampaha_dis']/$res_tot_gampaha_employee*100),2) ?>%</td>

            <?php }  ?>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_gampaha_employee ?></td>
            <td>100%</td>
          </tr>

        </table>
        <!-- Gampha District -->

        <!-- Province wise -->
        <table class="table">
          <tr align="center" style="background-color: gray; color: #fff"><td colspan="4">Province Wise</td></tr>

          <?php 

          $get_province_row = "SELECT province, count(province) FROM tblemployee GROUP BY province";
          $run_province_row = mysqli_query($con,$get_province_row);
          $res_province_row = mysqli_num_rows($run_province_row);


          ?>

          <tr align="center" bgcolor="lightgray" style="font-weight: 500 ">
            <td width="175px">Province</td>
            <td width="175px">No. of Employees</td>
            <td width="175px">%</td>
            <td rowspan="<?php echo ($res_province_row + 2 ) ?>" bgcolor="#fff"><div id="piechart3" style="width: 100%; height: <?php echo (($res_province_row + 2 )*50) ?>px"></div></td>
          </tr>

          <?php 

          $get_province = "SELECT province, count(province) AS count_prov FROM tblemployee GROUP BY province";
          $run_province = mysqli_query($con,$get_province );

          while($res_province = mysqli_fetch_array($run_province )){

          ?>

          <tr align="center">
            <td><?php echo $res_province['province'] ?></td>
            <td><?php echo $res_province['count_prov'] ?></td>
            <td><?php echo round(($res_province['count_prov']/$res_tot_employee*100),2) ?>%</td>
          </tr>

          <?php
          }
          ?>

          <tr class="bg-danger text-light" align="center">
            <td>Total</td>
            <td><?php echo $res_tot_employee ?></td>
            <td>100%</td>
          </tr>
        </table>
        <!-- Province wise -->

        </div>
      </div>


		  
    </div>
</div>  


<!-- content -->

<!-- chart.js -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

    <!-- Gender wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Category', 'Products'],

         <?php 

			  // $tot_male = "SELECT * FROM tblemployee WHERE title = 'Mr.'";
     //    $run_male = mysqli_query($con,$tot_male);
     //    $res_male = mysqli_num_rows($run_male);

		    	?>
		    	['Male',<?php echo $res_male ?>],
          ['Female',<?php echo $res_female ?>],


        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>
    <!-- Gender wise -->

      <!-- District wise -->
      <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Category', 'Products'],

         <?php 

        $get = "SELECT district, count(district) AS count FROM tblemployee GROUP BY district";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['district'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart1'));

        chart.draw(data, options);
      }
    </script>
    <!-- District wise -->

    <!-- Gampaha District -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Category', 'Products'],

         <?php 

        $get = "SELECT gampaha_district, count(gampaha_district) AS count FROM tblemployee GROUP BY gampaha_district";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          if(empty($res['gampaha_district'])) {

          }else{

          ?>
          ['<?php if(empty($res['gampaha_district'])) echo 'Pradeshiya saba not belongs to Gampaha District'; else echo $res['gampaha_district'] ?>',<?php echo $res['count'] ?>],

        <?php
        } }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart2'));

        chart.draw(data, options);
      }
    </script>
    <!-- Gampaha District -->

    <!-- Province wise -->
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Category', 'Products'],

         <?php 

        $get = "SELECT province, count(province) AS count FROM tblemployee GROUP BY province";
        $run = mysqli_query($con,$get);

        while($res = mysqli_fetch_array($run)){

          ?>
          ['<?php echo $res['province'] ?>',<?php echo $res['count'] ?>],

        <?php
        }
        ?>

        ]);

        var options = {
          title: ''
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart3'));

        chart.draw(data, options);
      }
    </script>
    <!-- Province wise -->
<!-- chart.js -->

<?php 
  include "inc/footer.php";
?>

