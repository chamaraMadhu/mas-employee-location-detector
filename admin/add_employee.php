<?php
session_start();
if(!isset($_SESSION['email'])){
   header('location:login.php');
}
if($_SESSION['role']=='Manager'){
    header('location:dashboard.php');
}
?>

<?php 
  include "inc/header.php";
  include "inc/slider.php";
?>

<!-- content -->
        <div class="col-10 bg-danger p-0">
             <div class="page-wrapper">

          <nav aria-label="breadcrumb">
              <ol class="breadcrumb bg-danger" style="font-size: 14px">
                  <li class="breadcrumb-item text-light active" aria-current="page">Employee</li>
                  <li class="breadcrumb-item active" aria-current="page"><a class="text-light" href="add_employee.php" style="text-decoration: none">Add Employee</a></li>
              </ol>
          </nav>

          <div class="container-fluid">

          <h2>EMPLOYEE</h2><hr>
                <?php
                  if(isset($_GET['success_msg'])){
                ?>
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong><?php echo $_GET['success_msg']; ?> </strong> 
                            </div>

                <?php }elseif(isset($_GET['fail_msg'])){ ?>

                    <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <strong><?php echo $_GET['fail_msg']; ?></strong> 
                            </div>
                <?php } ?>

                  <div class="container-fluid bg-white mb-4">

                    <div class="row pt-2" style="background-color: gray">
                        <h5 class="col-12 text-white">ADD EMPLOYEE</h5>
                    </div>

                    <form action="employee/add_employee_query.php" method="POST" enctype="multipart/form-data" class="needs-validation mt-3" novalidate>
                      <div class="form-row">

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Emp ID <b class="text-danger">*</b></label>
                          <div class="col-sm-2">
                            <input type="text" name="emp_id" class="form-control" id="validationCustom01" required>
                            <div class="invalid-feedback">
                            Please insert the employee ID.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Title <b class="text-danger">*</b></label>
                          <div class="col-sm-2">
                            <select name="title" class="form-control" id="validationCustom02" required>
                              <option></option>
                              <option>Mr.</option>
                              <option>Miss.</option>
                              <option>Mrs.</option>
                              <option>Ms.</option>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the title.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Name with Initials <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="text" name="name" class="form-control" id="validationCustom03" required>
                            <div class="invalid-feedback">
                            Please insert the name with initial.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Designation <b class="text-danger">*</b></label>
                          <div class="col-sm-4">
                            <input type="text" name="designation" class="form-control" id="validationCustom04" required>
                            <div class="invalid-feedback">
                            Please insert the designation.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Image </label>
                          <div class="col-sm-4">
                            <input type="file" name="img" class="form-control">
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Address <b class="text-danger">*</b></label>
                          <div class="col-sm-6">
                            <input type="text" name="address" class="form-control" id="validationCustom05" required>
                            <div class="invalid-feedback">
                            Please insert the address.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">District <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="district" class="form-control" id="district" onchange="select(this.id)" required>
                              <option></option>
                              <option>Colombo</option>
                              <option value="Gampaha">Gampaha</option>
                              <option>Kalutara</option>
                              <option>Matale</option>
                              <option>Kandy</option>
                              <option>Nuwara Eliya</option>
                              <option>Kegalle</option>
                              <option>Ratnapura</option>
                              <option>Puttalam</option>
                              <option>Kurunegala</option>
                              <option>Badulla</option>
                              <option>Monaragala</option>
                              <option>Trincomalee</option>
                              <option>Batticaloa</option>
                              <option>Ampara</option>
                              <option>Hambantota</option>
                              <option>Matara</option>
                              <option>Galle</option>
                              <option>Anuradhapura</option>
                              <option>Polonnaruwa</option>
                              <option>Jaffna</option>
                              <option>Kilinochchi </option>
                              <option>Mannar</option>
                              <option>Mullaitivu</option>
                              <option>Vavuniya</option>

                            </select>
                            <div class="invalid-feedback">
                            Please insert the district.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row" id="gampaha_dist">
                          <label class="col-sm-3 col-form-label text-right">Pradeshiya Saba <b class="text-danger">*</b></label>
                          <div class="col-sm-3">
                            <select name="gampaha_district" class="form-control"  id="validationCustom06">
                              <option></option>
                              <option>Attanagalla</option>
                              <option>Biyagama</option>
                              <option>Divulapitiya</option>
                              <option>Dompe</option>
                              <option>Gampaha</option>
                              <option>Ja-ela</option>
                              <option>Katana</option>
                              <option>Kelaniya</option>
                              <option>Mahara</option>
                              <option>Meerigama</option>
                              <option>Minuwangoda</option>
                              <option>Wattala</option>
                            </select>
                            <div class="invalid-feedback">
                            Please insert the Pradeshiya saba in Gampaha district.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Email </label>
                          <div class="col-sm-4">
                            <input type="email" name="email" class="form-control">
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Contact No </label>
                          <div class="col-sm-3">
                            <input type="text" name="home" class="form-control"  placeholder="Home">
                            <input type="text" name="mobi1" class="form-control mt-2" placeholder="Mobile 1">
                            <input type="text" name="mobi2" class="form-control mt-2" placeholder="Mobile 2">
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Map <b class="text-danger">*</b></label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="5" name="map" id="validationCustom08" required></textarea>
                            <div class="invalid-feedback">
                            Please insert the map.
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12 mb-3 form-group row">
                          <label class="col-sm-3 col-form-label text-right">Map Description </label>
                          <div class="col-sm-8">
                            <textarea class="form-control" rows="3" name="map_desc" ></textarea>
                          </div>
                        </div>

                        <div class="col-md-12 mb-0 form-group row">
                          <label class="col-sm-3 col-form-label text-right"></label>
                          <div class="col-sm-4">
                            <button class="btn btn-success btn-sm mb-3" type="submit" name="submit">Add Employee</button>
                          </div>
                        </div>

                        <div class="col-md-12 mb-4 form-group row">
                          <label class="col-sm-3 col-form-label text-right"></label>
                          <div class="col-sm-4">
                            <b class="text-danger" style="font-size: 14px">* Required</b>
                          </div>
                        </div>
                    </form>

                    </div>
                </div>      
              </div>    
            </div>
                
          </div>
        </div>  
   
     </div>
    <!-- content -->

<!-- selecting gampaha Pradeshiya saba -->
<script src="js/jquery-3.3.1.js"></script>
<script>
$("#gampaha_dist").hide();

function select(district){
  var dis = document.getElementById("district");

  if(dis.value=='Gampaha'){

      $("#gampaha_dist").show();

  }else{
      $("#gampaha_dist").hide();
  }

}
</script>
<!-- selecting gampaha Pradeshiya saba -->

<?php 
  include "inc/footer.php";
?>