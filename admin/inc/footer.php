    </div>
</div>
<!-- container-->  

<!-- footer -->
    <footer class="text-center text-light pt-1" style="font-size: 12px;height: 25px; background-color: #000">
        All rights reserved. Designed and Developed by Chamara Madhushanka <a href="http://www.resumecham.simplesite.com" target="_blank">resumecham.simplesite.com</a>

<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

<script>
// Example starter JavaScript for disabling form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>

 <!-- data table Files -->
<script src="../js/backend_js/datatables.min.js"></script>
<script>
  $('#zero_config').DataTable();
</script>
<!-- data table Files -->

<!-- popper -->
<script src="../js/backend_js/popper.min.js"></script>
<!-- popper -->

<!-- alert for employee deletion -->
<script>
    $(".del_emp").click(function(){
        if(confirm("Are you sure to delete this employee record?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for employee deletion -->

<!-- alert for employee deletion -->
<script>
    $(".del_emp_all").click(function(){
        if(confirm("Are you sure to delete all the employee record?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for employee deletion -->

<!-- alert for employee deletion -->
<script>
    $(".del_user").click(function(){
        if(confirm("Are you sure to delete this user?")){
            return true;
        }
        return false;
    });
</script>
<!-- alert for employee deletion -->

</body>
</html>

