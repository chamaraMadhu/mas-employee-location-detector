<!-- container-->
    <div class="container-fluid">
        <div class="row" style="min-height: 700px">

            <!-- sidebar -->
            <div class="col-sm-2 col-md-2 p-0" style="background-color: #000">
              <div id="side_bar">

                <p class="mb-1" style="margin-left: -25px; padding-top: 10px; font-size: 15px; font-weight: 600"><a href="dashboard.php">Dashboard</a></p>

                <h6 class="text-danger"  style="margin-left: 15px; font-size: 16px; font-weight: 600; margin-bottom: 10px">Employee</h6>

                <?php if(isset($_SESSION['email'])){ if($_SESSION['role']<>'Manager'){ if($_SESSION['add']=='1'){ ?>
                    <p  style="margin-bottom: 5px"><a href="add_employee.php">Add Employee</a></p>
                <?php } } } ?>

                <?php if(isset($_SESSION['email'])){ if($_SESSION['view']=='1'){ ?>
                    <p><a href="view_employee.php">View Employee</a></p>
                <?php } } ?>

              </div>
            </div>
            <!-- sidebar -->